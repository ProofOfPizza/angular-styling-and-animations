import {
  animate,
  AnimationTriggerMetadata,
  group,
  keyframes,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const showStateTrigger: AnimationTriggerMetadata = trigger('showState', [
  // state('shown', style({})),
  // transition('void => shown', [
  // transition('void => *', [
  transition(':enter', [
    style({ opacity: 0, borderColor: 'transparent', width: 0 }),
    animate(500),
  ]),
  transition(
    // 'shown => void',
    ':leave',
    animate(
      500,
      style({
        opacity: 0,
        borderColor: 'transparent',
      })
    )
  ),
]);

export const animateStateTrigger: AnimationTriggerMetadata = trigger(
  'animateState',
  [
    transition('* => *', [
      animate(
        '400ms cubic-bezier(.22,.02,.47,1.49)',
        style({
          width: 0,
        })
      ),
      animate(
        400,
        style({
          width: '*', // for dimesnions like width /height when unknown
        })
      ),
    ]),
  ]
);

export const listStateTrigger: AnimationTriggerMetadata = trigger('listState', [
  transition(':enter', [
    style({
      background: 'white',
      opacity: 0,
      borderColor: 'transparent',
      width: 0,
      color: 'transparent',
    }),
    group([
      animate(400, style({ opacity: 0.6 })),
      animate(200, style({ width: '*' })),
      animate(150, style({ background: 'lightpink' })),
      animate(300, style({ color: 'magenta' })),
    ]),
    animate(400),
  ]),
  transition(
    ':leave',
    animate(
      500,
      style({
        opacity: 0,
        borderColor: 'transparent',
      })
    )
  ),
]);

export const listStateTrigger2: AnimationTriggerMetadata = trigger(
  'listState2',
  [
    transition(':enter', [
      style({
        background: 'white',
        opacity: 0,
        borderColor: 'transparent',
        width: 0,
        color: 'transparent',
      }),
      group([
        animate(400, style({ opacity: 0.6 })),
        animate(800, style({ width: '*' })),
        // animate(150, style({ background: 'lightpink' })),
        animate(
          1500,
          keyframes([
            style({
              background: 'white',
              offset: 0,
            }),
            style({
              background: 'green',
              offset: 0.2,
            }),
            style({
              background: 'black',
              offset: 0.8,
            }),
            style({
              background: 'lightpink',
              offset: 1,
            }),
          ])
        ),
        animate(700, style({ color: 'magenta' })),
      ]),
      animate(400),
    ]),
    transition(
      ':leave',
      animate(
        500,
        style({
          opacity: 0,
          borderColor: 'transparent',
        })
      )
    ),
  ]
);

