import { Component } from '@angular/core';
import {
  animateStateTrigger,
  listStateTrigger,
  listStateTrigger2,
  showStateTrigger,
} from './app.animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    showStateTrigger,
    animateStateTrigger,
    listStateTrigger,
    listStateTrigger2,
  ],
})
export class AppComponent {
  isShown = false;
  width = 400;
  animate = false;
  testresults = [];

  onAddElement() {
    this.testresults.push(Math.random());
  }

  onAnimationStart(event: AnimationEvent) {
    console.log(event);
  }

  onAnimationDone(event: AnimationEvent) {
    console.log(event);
  }
}

