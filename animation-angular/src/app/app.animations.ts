import {
  animate,
  AnimationTriggerMetadata,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const clickedStateTrigger: AnimationTriggerMetadata = trigger(
  'clickedState',
  [
    state(
      'default',
      style({
        backgroundColor: 'orange',
        width: '10rem',
        height: '10rem',
      })
    ),
    state(
      'clicked',
      style({
        backgroundColor: 'blue',
        width: '15rem',
        height: '15rem',
      })
    ),
    state(
      'mouseDown',
      style({
        backgroundColor: '#ff55',
        width: '7rem',
        height: '7rem',
        border: '4px dashed black',
      })
    ),
    transition('default => clicked', animate('400ms 100ms ease-out')),
    transition('clicked => default', animate(700)),
    transition('mouseDown <=> clicked', animate(700)),
    transition('mouseDown <=> default', animate(2000)),
    // transition('clicked => mouseDown', animate(700)),
  ]
);

export const numberEnteredStateTrigger: AnimationTriggerMetadata = trigger(
  'numberEnteredState',
  [
    state(
      'unselected',
      style({
        border: '1px solid black',
        padding: '5px',
      })
    ),
    state(
      'selected',
      style({
        border: '2px solid purple',
        padding: '4px',
        backgroundColor: 'lightblue',
      })
    ),
    transition('selected <=> unselected', [
      style({
        border: '2px solid black',
        padding: '4px',
        // transform: 'scale(1)',
      }),
      animate(
        300,
        style({
          backgroundColor: 'red',
          transform: 'scale(1.1)', //only for (inline)-block displays...see css file
        })
      ),
      animate(200),
    ]),
  ]
);

