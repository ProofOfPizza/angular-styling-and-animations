import { Component, ElementRef, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Dreams & Quotes';
  isFavourite = false;
  @ViewChild('boringBlock') boringBlock: ElementRef;

  constructor(private renderer: Renderer2) {}

  onShowBoring() {
    // this.boringBlock.nativeElement.style.display = 'block';
    // volgende notatie is beter omdat Angular soms ook dingen doet in andere dingen dan een normale DOM
    this.renderer.setStyle(this.boringBlock.nativeElement, 'display', 'block');
  }
}

