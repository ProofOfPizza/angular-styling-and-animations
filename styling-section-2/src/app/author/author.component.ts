import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  encapsulation: ViewEncapsulation.Emulated, //default
  styles: [
    `
      h1 {
        margin: 0;
        font-size: 12px;
        color: #38c8c8c;
      }
    `,
  ],
})
export class AuthorComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}

