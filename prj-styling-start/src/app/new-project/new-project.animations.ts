import {
  AnimationTriggerMetadata,
  animate,
  style,
  state,
  transition,
  trigger,
  query,
} from "@angular/animations";

export const buttonStateTrigger: AnimationTriggerMetadata = trigger(
  "buttonState",
  [
    state(
      "invalid",
      style({
        backgroundColor: "#b70909",
        borderColor: "darkred",
        transform: "scale(1)",
        boxShadow: "none",
      })
    ),
    state(
      "valid",
      style({
        backgroundColor: "#7cf466",
        borderColor: "black",
        color: "black",
        transform: "scale(1)",
        boxShadow: "none",
      })
    ),
    transition("valid => invalid", [
      animate(
        100,
        style({
          backgroundColor: "#b70909",
          transform: "scale(1.05)",
          boxShadow: "3px 3px 3px rgba(0,0,0,0.6)",
        })
      ),
      animate(200),
    ]),
    transition("invalid => valid", [
      animate(
        100,
        style({
          backgroundColor: "#7cf466",
          borderColor: "black:",
          color: "black",
          transform: "scale(1.05)",
          boxShadow: "3px 3px 3px rgba(0,0,0,0.6)",
        })
      ),
      animate(200),
    ]),
  ]
);

export const formStateTrigger: AnimationTriggerMetadata = trigger("formState", [
  transition("* => *", [
    query(
      "input.ng-invalid:focus",
      [
        animate(
          200,
          style({
            background: "red",
          })
        ),
        animate(200),
      ],
      { optional: true }
    ),
  ]),
]);

