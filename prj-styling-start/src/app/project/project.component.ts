import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  Renderer2,
  HostBinding,
  ViewChild,
  ElementRef,
} from "@angular/core";

import { Project } from "../projects/project.model";
import { markedStateTrigger } from "../projects/projects.animations";

@Component({
  selector: "app-project",
  templateUrl: "./project.component.html",
  styleUrls: ["./project.component.css"],
  // animations: [markedStateTrigger],
})
export class ProjectComponent implements OnInit {
  status: string = "";
  @Input() marked: boolean;
  @ViewChild("statusLabel") label: ElementRef;
  @Input() project: Project;
  @Output() statusUpdated = new EventEmitter<string>();
  @Output() projectDeleted = new EventEmitter<void>();
  @HostBinding("class") class = this.status;

  constructor(private renderer: Renderer2) {}

  ngOnInit(): void {
    this.statusUpdated.subscribe((status: string) => {
      this.class = this.marked ? `project__marked ${status}` : status;
      let color = "";
      if (status === "active") {
        color = "blue";
      }
      if (status === "inactive") {
        color = "#ccc";
      }
      if (status === "critical") {
        color = "red";
      }
      this.renderer.setStyle(this.label.nativeElement, "background", color);
    });
  }
  ngAfterContentChecked() {
    this.onUpdateStatus(this.project.status);
  }
  onUpdateStatus(newStatus: string) {
    this.statusUpdated.emit(newStatus);
  }

  onDelete() {
    this.projectDeleted.emit();
  }
}

