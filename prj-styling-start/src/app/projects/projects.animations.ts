import {
  AnimationTriggerMetadata,
  animate,
  trigger,
  keyframes,
  style,
  state,
  stagger,
  group,
  query,
  transition,
} from "@angular/animations";

export const slideNewProjectStateTrigger: AnimationTriggerMetadata = trigger(
  "slideNewProjectState",
  [
    transition(":enter", [
      style({
        transform: "translateY(-100%)",
      }),

      animate(
        "400ms ease-out",
        style({
          transform: "translateY(0)",
        })
      ),
    ]),
    transition(":leave", [
      animate(
        "400ms ease-out",
        style({
          transform: "translateY(-100%)",
        })
      ),
    ]),
  ]
);

export const slideListStateTriggerXXX: AnimationTriggerMetadata = trigger(
  "slideListStateXXX",
  [
    transition(":enter", [
      style({
        transform: "translateX(-100%)",
        opacity: 0,
      }),
      group([
        animate(
          "400ms ease-out",
          style({
            transform: "translateX(0)",
          })
        ),
        animate(
          "250ms ease-out",
          style({
            opacity: 1,
          })
        ),
      ]),
    ]),
    transition(":leave", [
      group([
        animate(
          "400ms ease-out",
          style({
            transform: "translateX(-100%)",
          })
        ),
        animate(
          "500ms ease-out",
          style({
            opacity: 0,
          })
        ),
      ]),
    ]),
  ]
);

export const slideListStateTrigger: AnimationTriggerMetadata = trigger(
  "slideListState",
  [
    transition(
      ":leave",
      animate(
        "450ms linear",
        keyframes([
          style({
            transform: "translateX(0)",
            opacity: 1,
            offset: 0,
          }),
          style({
            opacity: 0.7,
            transform: "translateX(-15%)",
            offset: 0.5,
          }),

          style({
            transform: "translateX(100%)",
            offset: 1,
          }),
        ])
      )
    ),
    transition(":leave", [
      group([
        animate(
          "400ms ease-out",
          style({
            transform: "translateX(-100%)",
          })
        ),
        animate(
          "500ms ease-out",
          style({
            opacity: 0,
          })
        ),
      ]),
    ]),
    transition("up => down", [
      style({
        transform: "translateY(-100%)",
      }),
      animate(
        "400ms ease-out",
        style({
          transform: "translateY(0)",
        })
      ),
    ]),
    transition("down => up", [
      style({
        transform: "translateY(0)",
      }),
      animate(
        "400ms ease-out",
        style({
          transform: "translateY(-100%)",
        })
      ),
    ]),
  ]
);

export const markedStateTrigger: AnimationTriggerMetadata = trigger(
  "markedState",
  [
    state("default", style({})),
    state(
      "marked",
      style({
        background: "#f4e9be",
      })
    ),
    transition("default => marked", [
      style({
        display: "block",
      }),
      animate(
        150,
        style({
          transform: "scale(1.1)",
          backgroundColor: "#f5facf",
        })
      ),
      animate(
        250,
        style({
          transform: "scale(1)",
          backgroundColor: "#f4e9be",
        })
      ),
    ]),
    transition("marked => default", animate("300ms ease-out")),
  ]
);

export const listStateTrigger: AnimationTriggerMetadata = trigger("listState", [
  transition("* => *", [
    query(
      ":enter",
      [
        style({
          transform: "translateX(-70%)",
          opacity: 0,
        }),
        stagger(240, [
          animate(
            "600ms ease-out",
            keyframes([
              style({
                opacity: 0.7,
                transform: "translateX(15%)",
                offset: 0.6,
              }),

              style({
                transform: "translateX(0)",
                offset: 1,
              }),
            ])
          ),
        ]),
      ],
      { optional: true }
    ),
  ]),
]);

