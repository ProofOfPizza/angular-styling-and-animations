import { HostBinding, Component, OnInit, Renderer2 } from "@angular/core";
import { AnimationEvent } from "@angular/animations";
import { Project } from "./project.model";
import {
  routeFadeStateTrigger,
  routeSlideStateTrigger,
} from "../shared/routes.animations";
import {
  markedStateTrigger,
  slideNewProjectStateTrigger,
  slideListStateTrigger,
  listStateTrigger,
} from "./projects.animations";
import { ProjectsService } from "./projects.service";

@Component({
  selector: "app-projects",
  templateUrl: "./projects.component.html",
  styleUrls: ["./projects.component.css"],
  animations: [
    listStateTrigger,
    markedStateTrigger,
    slideNewProjectStateTrigger,
    slideListStateTrigger,
    // routeFadeStateTrigger,
    routeSlideStateTrigger,
  ],
})
export class ProjectsComponent implements OnInit {
  // @HostBinding("@routeFadeState") routeAnimation = true;
  @HostBinding("@routeSlideState") routeAnimation = true;
  projects: Project[];
  markedPrjIndex = 0;
  progress = "progressing";
  createNew = false;

  constructor(
    private prjService: ProjectsService,
    private renderer: Renderer2
  ) {}

  ngOnInit() {
    this.prjService.loadProjects().subscribe((prj: Project[]) => {
      this.progress = "finished";
      this.projects = prj;
    });
  }

  onStatusUpdated(newStatus: string, id: number) {
    this.projects[id].status = newStatus;
  }

  onProjectDeleted(index: number) {
    this.projects.splice(index, 1);
  }

  onProjectCreated(project: Project) {
    this.createNew = false;
    setTimeout(() => {
      this.projects.unshift(project);
    }, 400);
  }
}

