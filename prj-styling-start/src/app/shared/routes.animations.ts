import {
  AnimationTriggerMetadata,
  AnimationReferenceMetadata,
  animate,
  trigger,
  transition,
  style,
  group,
  animation,
  useAnimation,
} from "@angular/animations";

export const fadeAnimation: AnimationReferenceMetadata = animation(
  [
    style({
      opacity: "{{ startOpacity }}",
    }),
    animate("{{ duration }}"),
  ],
  { params: { startOpacity: 0, duration: "100ms" } }
);

export const routeFadeStateTrigger: (
  params: any
) => AnimationTriggerMetadata = (params: {
  startOpacity: number;
  duration: string;
}) =>
  trigger("routeFadeState", [
    transition(":enter", [useAnimation(fadeAnimation, { params: params })]),
    transition(
      ":leave",
      animate(
        300,
        style({
          opacity: 0,
        })
      )
    ),
  ]);

export const routeSlideStateTrigger: AnimationTriggerMetadata = trigger(
  "routeSlideState",
  [
    transition(":enter", [
      style({
        transform: "translateY(-100%)",
        opacity: 0,
      }),
      animate(300),
    ]),
    transition(":leave", [
      group([
        animate(
          180,
          style({
            opacity: 0.3,
          })
        ),
        animate(
          300,
          style({
            transform: "translateY(100%)",
            opacity: 0,
          })
        ),
      ]),
    ]),
  ]
);

