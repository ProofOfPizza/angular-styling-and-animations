import { Component } from "@angular/core";
import { RouterOutlet } from "@angular/router";
import {
  trigger,
  transition,
  query,
  style,
  animateChild,
  animate,
  group,
} from "@angular/animations";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
  animations: [
    trigger("routeState", [
      // transition("* => *", [
      transition("rootPage => usersPage", [
        group([
          query(
            ":enter",
            [
              // animateChild(), if we would want to STILL use child component animations which are now overridden
              style({
                transform: "translateY(-100%)",
                opacity: 0,
              }),
              animate("300ms ease-out"),
            ],
            { optional: true }
          ),
          query(
            ":leave",
            [
              animate(
                "300ms ease-out",
                style({
                  transform: "translateY(100%)",
                  opacity: 0,
                })
              ),
            ],
            { optional: true }
          ),
        ]),
      ]),
      transition("usersPage => rootPage", [
        group([
          query(
            ":enter",
            [
              // animateChild(), if we would want to STILL use child component animations which are now overridden
              style({
                transform: "translateY(100%)",
                opacity: 0,
              }),
              animate("300ms ease-out"),
            ],
            { optional: true }
          ),
          query(
            ":leave",
            [
              animate(
                "300ms ease-out",
                style({
                  transform: "translateY(-100%)",
                  opacity: 0,
                })
              ),
            ],
            { optional: true }
          ),
        ]),
      ]),
    ]),
  ],
})
export class AppComponent {
  getAnimationData(outlet: RouterOutlet) {
    const routeData = outlet.activatedRouteData["animation"];
    if (!routeData) {
      return "rootPage";
    }
    return routeData["page"];
  }
}

